package inheritance.analyser;

import java.util.List;

public class FlatTaxSalesAnalyser {

    public FlatTaxSalesAnalyser(List<SalesRecord> records) {
        throw new RuntimeException("not implemented yet");
    }

    public Double getTotalSales() {
        throw new RuntimeException("not implemented yet");
    }

    public Double getTotalSalesByProductId(String id) {
        throw new RuntimeException("not implemented yet");
    }

    public List<String> getTop3PopularItems() {
        throw new RuntimeException("not implemented yet");
    }

    public String getIdOfItemWithLargestTotalSales() {
        throw new RuntimeException("not implemented yet");
    }

}
